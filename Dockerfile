FROM amazoncorretto:20
COPY ./build/libs/ ./
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "*"]